#!/bin/bash

if [ ! $(ulimit -n) -gt 1024 ]
then
echo "on wsl2, the default of 1024 fileno (max. number of open file handles) can be problematic"
echo "to raise for current session, use 'sudo prlimit --nofile=80000 --pid \$\$'"
exit 0
fi    
    
if [ -z "$MACHINE" ]; then
    echo "MACHINE not set. Defaulting to fccp!"
    MACHINE=fccp
fi

echo "Building yocto for $MACHINE"

COMMAND="$@"
if [ -z "$COMMAND" ]; then
    COMMAND="bitbake core-image-serenergy"
    echo "Using default image build command \"$COMMAND\""
else
    echo "Using build command \"$COMMAND\""
fi

SCRIPT_DIR=`dirname "${BASH_SOURCE[0]}"`
SCRIPT_DIR=`realpath $SCRIPT_DIR`

# some docker options to allow the container to use the users ssh known_hosts file and ssh agent socket for automatic ssh authentication against private git repos
SSH_AGENT_DOCKER_OPTS="-v $(realpath ~/.ssh/known_hosts):/etc/ssh/ssh_known_hosts -v $(dirname $SSH_AUTH_SOCK):$(dirname $SSH_AUTH_SOCK) -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK"

if [ -z "$BUILD_DIR" ]; then
    BUILD_DIR=build
fi

BUILD_COMMAND="source serenergy-init-build-env \"$BUILD_DIR\" && $COMMAND"

if which podman 1>/dev/null 2>&1; then
    echo "Running build command with podman"
    podman run --rm -it $SSH_AGENT_DOCKER_OPTS --security-opt label=disable \
        --userns=keep-id --user=$(id -u):$(id -g) -v $SCRIPT_DIR:/work --workdir=/work \
        -e BB_ENV_PASSTHROUGH_ADDITIONS=REPRODUCIBLE_TIMESTAMP_ROOTFS \
        -e REPRODUCIBLE_TIMESTAMP_ROOTFS=`git log -1 --pretty=%ct 2>/dev/null` \
        -e MACHINE=$MACHINE --entrypoint="" \
        crops/poky /bin/bash -c "$BUILD_COMMAND"
else
    systemctl is-active --quiet docker
    if [ $? != 0 ]; then
        echo "Docker daemon not running, please start it!"
        exit 1
    fi
    echo "Running build command in docker container. This requires sudo, so I might ask for you password."
    sudo -E docker run --rm -it $SSH_AGENT_DOCKER_OPTS -v $SCRIPT_DIR:/work \
        -e BB_ENV_PASSTHROUGH_ADDITIONS=REPRODUCIBLE_TIMESTAMP_ROOTFS \
        -e REPRODUCIBLE_TIMESTAMP_ROOTFS=`git log -1 --pretty=%ct 2>/dev/null` \
        -e MACHINE=$MACHINE crops/poky --workdir=/work /bin/bash -c "$BUILD_COMMAND"
fi

if [ $? != 0 ]; then 
    echo "Yocto build command failed!"
    exit 1
fi
echo "Yocto build command finished succesfully"



