# This script is inspired of the following reference:
# https://low-level.wiki/mirrors/yocto_sources.html

source serenergy-init-build-env build
grep BB_GENERATE_MIRROR_TARBALLS conf/local.conf 

# make sure we have generation of tarballs for all git and svn
if [ $? != 0 ]; then
    echo "Update local.conf file"
    echo 'BB_GENERATE_MIRROR_TARBALLS = "1"' >> conf/local.conf
fi
MACHINE=rpi3-frontend bitbake feb-package --runall fetch && \
MACHINE=rpi3-frontend  bitbake core-image-serenergy -c populate_sdk --runall fetch && \
MACHINE=fccp  bitbake fccp-package --runall fetch && \
MACHINE=fccp  bitbake core-image-serenergy -c populate_sdk --runall fetch && \
MACHINE=container-x86-64 bitbake container-package --runall fetch && \
MACHINE=container-x86-64 bitbake base-container -c populate_sdk --runall fetch

if [ $? != 0 ]; then
    echo "Failed to run all receipies and download all source code: err code: $?"
    exit 1
fi

cd ..
# clean up in downloads folder
echo "Clean up downloads"
find ./downloads -name "*bad-checksum*" -exec rm -f {} \;
rm -rf ./downloads/git2
rm -rf ./downloads/svn
find ./downloads -name "*.done" -exec rm -f {} \;
find ./downloads -name "*.lock" -exec rm -f {} \;

echo "Please copy all files from downloads to the local http server"

