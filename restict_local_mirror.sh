# This script is inspired of the following reference:
# https://low-level.wiki/mirrors/yocto_sources.html

source serenergy-init-build-env build
grep BB_ALLOWED_NETWORKS conf/local.conf 

# make sure we have generation of tarballs for all git and svn
if [ $? != 0 ]; then
    echo "Update local.conf file"
    echo 'PREMIRRORS:prepend = "\
git://.*/.* http://builddata.serenergy.local/yocto-src-downloads/ \
ftp://.*/.* http://builddata.serenergy.local/yocto-src-downloads/ \
http://.*/.* http://builddata.serenergy.local/yocto-src-downloads/ \
https://.*/.* http://builddata.serenergy.local/yocto-src-downloads/"

BB_ALLOWED_NETWORKS = "bitbucket.org/serenergy builddata.serenergy.local"
' >> conf/local.conf
    (cd ../sources/poky && patch -p1 < ../../0001-Add-path-control-to-BB_ALLOWED_NETWORKS.patch )
fi

echo "Ready to run bitbake"

